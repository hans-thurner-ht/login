<!DOCTYPE html>
<html>
    <head>
        <title>Login mit PHP</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    
    <body> 
        <?php
            session_start();
            $menu=3;
        include('menu.php');
            if (isset($_COOKIE['User']))  {
            if ($_COOKIE['User']==$user && $_COOKIE['Pwd']==$pwd) {
                $_SESSION['User']=$_COOKIE['User'];
                $_SESSION['Pwd']=$_COOKIE['Pwd'];
            }
        }
  
        if ($_SESSION==array())  {
		echo <<<END_1
            <h1>Mini Formular</h1>
            <form method='post'>
                User:<input type='text' name='User' /><br />
                Passwort:<input type='password' name='Pwd' /><br />
            	Angemeldet bleiben:<input type='checkbox' name='stay' value='stay_logged'/><br />
                <input type='submit' name='submit' value='Los!' />
            </form>
END_1;
        }


            if (isset($_POST['User']) )  {
            if (($_POST['User']==$user && $_POST['Pwd']==$pwd)) {
                if (isset($_POST['stay'])) {
                    setcookie('User', $_POST['User'], time()+600);
                    setcookie('Pwd', $_POST['Pwd'], time()+600);
                }
                $_SESSION['User']=$_POST['User'];
                $_SESSION['Pwd']=$_POST['Pwd'];
    			header('Location: Patientendatenabrufen.php');
            }
            else echo "Username oder Passwort falsch!";
        }
        ?>
    </body>
</html>