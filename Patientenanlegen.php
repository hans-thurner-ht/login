<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <title></title>
    </head>
    <body>
		<?php
		session_start();
		if (isset($_SESSION['User'])) {
			if (!isset($_SESSION['patientendaten'][0])) {
				$_SESSION['patientendaten'] = new \Ds\Vector();
			}
			$menu = 0;

			include('menu.php');
			$daten = ["Vorname", "Nachname", "SVNR", "Geschlecht", "Adresse", "Telefonnr"];
			echo '<form method="GET">';
			echo '<table class="table">';
			foreach ($daten as $field) {
				echo '<tr><td>' . $field . ':</td><td><input ' . ($field == "Geschlecht" ? 'type="text"' : 'type="text"') . 'name="' . $field . '"></td></tr></br>';
			}
			echo '<tr><td><input type="submit" value="Speichern"></td></tr>';
			echo '</form>';

			if (isset($_GET['Vorname']) && $_GET["Vorname"] != "") {
				$_SESSION['patientendaten']->push($_GET);
			}
		}
		?>
    </body>
</html>
