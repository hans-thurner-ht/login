<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <title></title>
    </head>
    <body>
		<?php
		session_start();
		if (isset($_SESSION['User'])) {
			$menu = 1;
			$id = 0;
			include('menu.php');
			echo '<br><table class="table">';
			echo '<thead><tr>';
			foreach (["Vorname", "Nachname", "SVNR", "Geschlecht", "Adresse", "Telefonnr", "Löschen"] as $header) {
				echo '<th>' . $header . '</th>';
			}
			if (isset($_SESSION['patientendaten'])) {
				foreach ($_SESSION['patientendaten'] as $value) {
					echo '<tr>';
					foreach ($value as $entry) {
						echo '<td>' . $entry . '</td>';
					}
					echo '<td><a href="deleteentry.php?id=' . $id . '">x</a>';
					echo '</tr>';
					$id++;
				}
				echo '</table><br>';
			}
		}
		?>
    </body>
</html>
